import React from 'react'

import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import DetailsCocktailScreen from '../screens/DetailsCocktailScreen';
import SearchCocktailScreen from '../screens/SearchCocktailScreen';

import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Ionicons';

const tabNavigator = createMaterialBottomTabNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            tabBarLabel: 'Accueil',
            tabBarIcon: ({ tintColor }) => (
                <Icon color={tintColor} size={25} name={'ios-home'} />
            )
        }
    },
    Details: {
        screen: DetailsScreen,
        navigationOptions:{
            tabBarLabel: 'DETAILS RANDOM',
            barStyle: {backgroundColor: 'blue'}
        }
    },
    DetailsCocktail: {
        screen: DetailsCocktailScreen,
        navigationOptions:{
            tabBarLabel: 'DETAILS',
            barStyle: {backgroundColor: 'blue'}
        }
    },
    SearchCocktail: {
        screen: SearchCocktailScreen,
        navigationOptions:{
            tabBarLabel: 'SEARCH',
            barStyle: {backgroundColor: 'blue'}
        }
    },
},
    {
        initialRouteName: 'Home'
    }
);

export default createAppContainer(tabNavigator);