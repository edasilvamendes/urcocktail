import React, { Component } from 'react'
import { StyleSheet, Button, View, Text } from 'react-native';
import { Container, Content, List } from 'native-base';
import { Image } from 'react-native-elements';

const styles = StyleSheet.create({
    contrainerCocktails: { backgroundColor: '#E0E0E0' },
    contrainerCocktail: {backgroundColor: '#FFFFFF',  marginTop: 15, marginBottom: 15, marginLeft: 15, marginRight: 15, borderRadius: 10, overflow: 'hidden',},
    nameStyle: { marginBottom: 10, marginTop: 10, fontSize: 18, fontWeight: 'bold', paddingLeft: 5, },
    infoStyle: { paddingLeft: 5, fontSize: 14, marginBottom: 5, },
});

export default class SearchCocktailScreen extends Component {

    state = {}

    back = () => {
        this.props.navigation.goBack();
    }

    render() {

        const { navigate } = this.props.navigation;
        const cocktailByName = this.props.navigation.state.params.detailsCocktailSearch;

        return (
            <View>
                <Button title="Back" 
                    onPress={this.back}
                />

                <List style={styles.contrainerCocktails}
                    dataArray={cocktailByName.drinks}
                    renderRow={(item) => {
                    return (
                        <View key={item.idDrink} style={styles.contrainerCocktail}>
                            <Image key={item.idDrink} source={{ uri: item.strDrinkThumb }} style={{ height: 350, width: 350 }} />
                            <Text key={item.idDrink} style={{marginBottom: 10}} style={styles.nameStyle}>{item.strDrink}</Text>
                            <Text key={item.idDrink} style={{marginBottom: 10}} style={styles.infoStyle}>Category: {item.strCategory}</Text>
                            <Text key={item.idDrink} style={{marginBottom: 10}} style={styles.infoStyle}>Etat: {item.strAlcoholic}</Text>
                            <Text key={item.idDrink} style={{marginBottom: 10}} style={styles.infoStyle}>Recipe: {item.strInstructions}</Text>
                            <Text key={item.idDrink} style={{marginBottom: 10}} style={styles.infoStyle}>Ingredients: {item.strIngredient1} / {item.strIngredient2} / {item.strIngredient3}</Text> 
                        </View>
                    )
                }} /> 
            </View>
        )
    }
}
