import React, { Component } from 'react';
import { CheckBox, TextInput, StyleSheet, Text, View, FlatList, Button } from 'react-native';
import { Container, Content, List } from 'native-base';
import CocktailService from '../services/CocktailService';
import { Image } from 'react-native-elements';

const styles = StyleSheet.create({
    contrainerCocktails: { backgroundColor: '#E0E0E0' },
    searchStyle: { height: 40, backgroundColor: '#BDBDBD', borderWidth: 1, borderColor: '#000000', paddingLeft: 5,},
    contrainerCocktail: {backgroundColor: '#FFFFFF',  marginTop: 15, marginBottom: 15, marginLeft: 15, marginRight: 15, borderRadius: 10, overflow: 'hidden',},
    nameStyle: { marginBottom: 10, marginTop: 10, fontSize: 18, fontWeight: 'bold', paddingLeft: 5, },
});

class HomeScreen extends Component {

    state = { 
        data: 'null',
        randomCocktail: 'null',
        detailsCocktail: 'null',
        nameCocktail: 'null',
        detailsCocktailSearch: 'null',
        checked: true,
    };

    serv = new CocktailService();

    componentDidMount() {
        this.serv.getCocktails().then(resp => {
            this.setState({ data: resp.data });
        });
    
        this.serv.getRandomCocktail().then(resp => {
            this.setState({ randomCocktail: resp.data });
        });
    }

    ChangeCocktailFunction = () => {
        const { navigate } = this.props.navigation;

        this.serv.getRandomCocktail().then(resp => { 
            navigate("Details", {randomCocktail: resp.data});
        });
    }

    findCocktailDetails(idDrink){
        const { navigate } = this.props.navigation;

        this.serv.getCocktailDetails(idDrink).then(resp => { 
            navigate("DetailsCocktail", {detailsCocktail: resp.data});
        });
    }

    onChangeNameCocktail = (value) => {
        this.setState({ nameCocktail: value });
    }

    findCocktailSearch = () => {
        const { navigate } = this.props.navigation;

        //BY NAME
        this.serv.getCocktailBySearch(this.state.nameCocktail, this.state.checked).then(resp => { 
            navigate("SearchCocktail", {detailsCocktailSearch: resp.data});
        });
    }

    onChangeCheckbox = () => {
        this.setState({ checked: !this.state.checked });     
    }

    render() {
        const { navigate } = this.props.navigation;

        return (
            <View>
                {this.state.randomCocktail.drinks ? (
                    <FlatList data={this.state.randomCocktail.drinks} 
                        renderItem={(e) => (
                        <Button title="Find random Cocktail" onPress={this.ChangeCocktailFunction} />
                    )} />
                ) : (<View/>)}

                <View>
                    <TextInput onChangeText={this.onChangeNameCocktail} style={styles.searchStyle} placeholder="Write a Cocktail name" placeholderTextColor = "#000000" />
                    <Button title="Search" onPress={this.findCocktailSearch} style={styles.btnStyle}/>
                </View>

                <List style={styles.contrainerCocktails}
                    dataArray={this.state.data.drinks}
                    renderRow={(item) => {
                    
                    return (
                        <View key={item.idDrink} style={styles.contrainerCocktail}>
                            <Image key={item.idDrink} source={{ uri: item.strDrinkThumb }} style={{ width: '100%', height: 350,}} />
                            <Text key={item.idDrink} style={styles.nameStyle}>{item.strDrink}</Text>
                            <Button key={item.idDrink} style={{ marginBottom: 50 }}
                                title="More"
                                onPress={()=>this.findCocktailDetails(item.idDrink)}
                            />   
                        </View>
                    )
                }} />
            </View>
        )
    }

}

export default HomeScreen
