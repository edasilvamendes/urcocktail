import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainTabNavigator from './navigation/MainTabNavigator';

class App extends React.Component {

  render() {
    return (
        <MainTabNavigator />
    );
  }
}

export default App;
