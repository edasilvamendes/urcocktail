import axios from 'axios';

const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail`;
const urlRandom = `https://www.thecocktaildb.com/api/json/v1/1/random.php`;
const urlCocktailDetail = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=`;
const urlCocktailName = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=`;

class CocktailService {

    //FOR ALL COCKTAIL
    getCocktails() {
        return axios.get(`${url}`);
    }
    
    //FOR RANDOM COCKTAIL
    getRandomCocktail() {
        return axios.get(`${urlRandom}`);
    }

    //FOR DETAILS
    getCocktailDetails(idDrink) {
        return axios.get(`${urlCocktailDetail}${idDrink}`);
    }

    //BY SEARCH
    getCocktailBySearch(nameCocktail) {
        return axios.get(`${urlCocktailName}${nameCocktail}`);
    }
    
}

export default CocktailService;